/**
 * Result provide a Result Monad Typescript implementations
 * maintainer: Soriyath Straessle <soriyath@leading.works>
 * Copyright (C) 2022 Leading Works SàRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * T is the desired value from an operation that can succeed or Failure
 * R is the shape of the Reason given upon failure, which can be stacked
 */
export type Result<T, R> = Success<T> | Failure<R>

/**
 * Developers can customize how the Result Monad logs their Failure Reasons upon
 * inspection.
 * The R generic type represent the shape used to stack Reasons.
 * It defaults to a console log of a Reason, and assume the Reason is a string by
 * default.
 */
export type Writer<R> = (reason: R) => void

/**
 * defaultWriter implements the Writer interface and logs out a Reason given for
 * a Failure. It assumes the Reason is either a string, or the Reason type
 * implements the .toString() interface.
 */
export const defaultWriter: Writer<any> = (reason: any) => console.log(reason)

export class Success<T> {
  // #region Properties and constructor
  __value__: T

  __writer__: any // overlaps with the Failure interface

  constructor (value: T) {
    this.__value__ = value
  }
  // #endregion

  // #region Functor
  /**
   * map :: f a -> (a -> b) -> f b
   * applies a function to the wrapped value in the Result Monad
   * @param f a {@link Function} that accepts the type T wrapped in the Result Monad
   * @returns a {@link Result} Monad with the returned value from the function f
   * @see https://en.wikibooks.org/wiki/Haskell/The_Functor_class
   */
  map <T>(f: Function): Success<T> {
    return Success.of(f(this.__value__))
  }
  // #endregion

  // #region Applicative Functor
  /**
   * ap applies an operation in a Success to another Success.
   * @param b a {@link Success} that wrap some value
   * @returns a {@link Success} with the result of the operation stored in the current object, applied to another {@link Success}
   * @see https://en.wikibooks.org/wiki/Haskell/Applicative_functors
   */
  ap <B>(b: Success<B>): Success<B> {
    return Success.of((this.join() as unknown as Function)(b.join()))
  }

  liftA2 <T>(fn: Function, f2: Success<T>): Success<T> {
    const curried: (a: T) => (b: T) => T = a => b => fn(a, b)
    return this.map(curried).ap(f2)
  }

  liftA3 <T>(fn: Function, f2: Success<T>, f3: Success<T>): Success<T> {
    const curried: (a: T) => (b: T) => (c: T) => T = a => b => c => fn(a, b, c)
    return this.map(curried).ap(f2).ap(f3)
  }

  liftA4 <T>(fn: Function, f2: Success<T>, f3: Success<T>, f4: Success<T>): Success<T> {
    const curried: (a: T) => (b: T) => (c: T) => (d: T) => T = a => b => c => d => fn(a, b, c, d)
    return this.map(curried).ap(f2).ap(f3).ap(f4)
  }

  liftA5 <T>(fn: Function, f2: Success<T>, f3: Success<T>, f4: Success<T>, f5: Success<T>): Success<T> {
    const curried: (a: T) => (b: T) => (c: T) => (d: T) => (e: T) => T = a => b => c => d => e => fn(a, b, c, d, e)
    return this.map(curried).ap(f2).ap(f3).ap(f4).ap(f5)
  }
  // #endregion

  // #region Monad type class
  chain <B>(f: (a: T) => Success<B>): Success<B> {
    return this.map(f).join() as Success<B>
  }

  bind = this.chain

  flatMap = this.chain
  // #endregion

  // #region Convenience methods
  inspect <T>(): Success<T> {
    console.log(`Success<${typeof this.__value__}>(${(this.__value__) as unknown as string})`)
    return this as unknown as Success<T> // Make it fluent for debugging purposes
  }

  unwrap (): T {
    return this.__value__
  }

  join (): T {
    return this.__value__
  }

  getReasons = this.join

  isSuccess (): boolean {
    return true
  }

  isFailure (): boolean {
    return false
  }
  // #endregion

  // #region Constructor convenience helpers
  of = Success.of

  return = Success.return

  pure = Success.pure

  unit = Success.unit

  static of <T>(value: T): Success<T> {
    return new Success<T>(value)
  }

  static return <T>(value: T): Success<T> {
    return new Success<T>(value)
  }

  static pure <T>(value: T): Success<T> {
    return new Success<T>(value)
  }

  static unit <T>(value: T): Success<T> {
    return new Success<T>(value)
  }
  // #endregion
}

/**
 * Failure is a construct that encapsulate a List of Reasons for a Failure to do
 * some operation.
 * @param R a generic type representing a Reason for a Failure.
 */
export class Failure<R> {
  // #region Properties and constructor
  __value__: R[] // a list of Reasons

  __writer__: Writer<R> // where R is the format for the Reasons to stack

  constructor (value: R, writer: Writer<R> = defaultWriter) {
    this.__value__ = [value]
    this.__writer__ = writer
  }
  // #endregion

  // #region Functor
  /**
   * map :: f a -> (a -> b) -> f b
   * applies a function to the wrapped value in the Result Monad, for the Failure
   * instance, it applies the function to each Reason in the stack (for formatting
   * or to anonymize logs for example).
   *
   * @param f a {@link Function} that accepts the type T wrapped in the Result Monad
   * @returns the {@link Failure} Monad with the stack of Failure Reasons, after calling the function on each Reason
   * @see https://en.wikibooks.org/wiki/Haskell/The_Functor_class
   */
  map <B>(f: Function): Failure<B> {
    const reasons = this.__value__.map((reason: R) => f(reason) as B)
    return Failure.of(reasons[0]).setReasons(reasons)
  }
  // #endregion

  // #region Applicative Functor
  /**
   * ap applies an operation in a Failure to another Failure.
   *
   * because Failure has a state (the stack of Reasons), such a case warrants
   * doing the operation, instead of default to a brand new Failure (such as what
   * happens with Maybe's Nothing for example).
   * we naively assume the Failure that is applied contains only functions, and map
   * over them unto all the stacked Reasons in the other Failure.
   *
   * Developers may probably use other Applicative Functor instance (e.g. Maybe)
   * to lift an operation.
   *
   * @param b a {@link Failure} that wrap some value
   * @returns a {@link Failure} with the result of the operation stored in the current object, applied to another {@link Success}
   * @see https://en.wikibooks.org/wiki/Haskell/Applicative_functors
   */
  ap <B>(b: Failure<B>): Failure<B> {
    const stack = this.__value__
    const reasons = b.join().flatMap((other: B, _idx, _arr) => {
      return stack.map((fn: R, _idx2, _arr2) => {
        return (fn as unknown as Function)(other)
      })
    }) as B[]
    return Failure<B>.ofReasons(reasons)
  }

  liftA2 <T>(fn: Function, f2: Failure<T>): Failure<T> {
    const curried: (a: T) => (b: T) => T = a => b => fn(a, b)
    return this.map(curried).ap(f2)
  }

  liftA3 <T>(fn: Function, f2: Failure<T>, f3: Failure<T>): Failure<T> {
    const curried: (a: T) => (b: T) => (c: T) => T = a => b => c => fn(a, b, c)
    return this.map(curried).ap(f2).ap(f3)
  }

  liftA4 <T>(fn: Function, f2: Failure<T>, f3: Failure<T>, f4: Failure<T>): Failure<T> {
    const curried: (a: T) => (b: T) => (c: T) => (d: T) => T = a => b => c => d => fn(a, b, c, d)
    return this.map(curried).ap(f2).ap(f3).ap(f4)
  }

  liftA5 <T>(fn: Function, f2: Failure<T>, f3: Failure<T>, f4: Failure<T>, f5: Failure<T>): Failure<T> {
    const curried: (a: T) => (b: T) => (c: T) => (d: T) => (e: T) => T = a => b => c => d => e => fn(a, b, c, d, e)
    return this.map(curried).ap(f2).ap(f3).ap(f4).ap(f5)
  }
  // #endregion

  // #region Monad type class
  chain <B>(f: (a: R) => Failure<B>): Failure<B> {
    const newState: B[] = []
    const failures = this.__value__.map(f)
    const nestedReasons = failures.map(mb => mb.join())
    nestedReasons.forEach(reasons => reasons.forEach(reason => newState.push(reason)))
    return Failure<B>.ofReasons(newState)
  }

  bind = this.chain

  flatMap = this.chain
  // #endregion

  // #region Convenience methods
  inspect (): Failure<R> {
    console.group('Failure')
    this.__value__.forEach((reason: R) => this.__writer__(reason))
    console.groupEnd()
    return this
  }

  unwrap (): R[] {
    return this.__value__
  }

  join (): R[] {
    return this.__value__
  }

  isSuccess (): boolean {
    return false
  }

  isFailure (): boolean {
    return true
  }

  setWriter (writer: Writer<R>): void {
    this.__writer__ = writer
  }

  setReasons (reasons: R[]): Failure<R> {
    this.__value__ = reasons
    return this
  }
  // #endregion

  // #region Constructor convenience helpers
  of = Failure.of

  ofReasons = Failure.ofReasons

  return = Failure.return

  pure = Failure.pure

  unit = Failure.unit

  static of <R>(reason: R): Failure<R> {
    return new Failure(reason)
  }

  static ofReasons <R>(reasons: R[]): Failure<R> {
    return new Failure(reasons[0]).setReasons(reasons)
  }

  static return <R>(reason: R): Failure<R> {
    return new Failure(reason)
  }

  static pure <R>(reason: R): Failure<R> {
    return new Failure(reason)
  }

  static unit <R>(reason: R): Failure<R> {
    return new Failure(reason)
  }
  // #endregion
}
