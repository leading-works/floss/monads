/**
 * result provide a Result Monad Typescript implementation
 * maintainer: Soriyath Straessle <soriyath@leading.works>
 * Copyright (C) 2022 Leading Works SàRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Success, Failure } from './result'

describe('Functor type class', () => {
  describe('Success instance', () => {
    it('honours the identity law', () => {
      expect(Success.of(42).map((t: number) => t))
        .toStrictEqual(Success.of(42))
    })
    it('honours the associativity law', () => {
      const compose: (f: Function) => (g: Function) => (x: number) => number = f => g => x => f(g(x))
      const increment: (a: number) => number = a => a + 1
      const double: (a: number) => number = a => a * 2
      const composed = compose(increment)(double)
      expect(Success.of(42).map(composed))
        .toStrictEqual(Success.of(42).map(double).map(increment))
    })
  })
  describe('Failure instance', () => {
    it('honours the identity law', () => {
      expect(Failure.of('division by zero').map((t: number) => t))
        .toStrictEqual(Failure.of('division by zero'))
    })
    it('honours the associativity law', () => {
      const compose: (f: Function) => (g: Function) => (x: number) => number = f => g => x => f(g(x))
      const increment: (a: number) => number = a => a + 1
      const double: (a: number) => number = a => a * 2
      const composed = compose(increment)(double)
      expect(Failure.of('division by zero').map(composed))
        .toStrictEqual(Failure.of('division by zero').map(double).map(increment))
    })
  })
})

describe('Applicative Functor type class', () => {
  describe('Success instance', () => {
    // pure id <*> v = v
    it('honours the identity law', () => {
      const identity: <T>(t: T) => T = t => t
      expect(Success.pure(identity).ap(Success.of(42)))
        .toStrictEqual(Success.pure(42))
    })
    // pure f <*> pure x = pure (f x)
    it('honours the homomorphism law', () => {
      const double: (a: number) => number = a => a * 2
      expect(Success.pure(double).ap(Success.pure(42)))
        .toStrictEqual(Success.pure(double(42)))
    })
    // u <*> pure y = pure ($ y) <*> u
    it('honours the interchange law', () => {
      type NbFn = (a: number) => number
      const double: NbFn = a => 2 * a
      const Double = Success.pure(double)
      // invert the order
      const interchange: (y: number) => (f: NbFn) => number = y => f => f(y)
      expect(Double.ap(Success.pure(42)))
        .toStrictEqual(Success.pure(interchange(42)).ap(Double))
    })
    // pure (.) <*> u <*> v <*> w = u <*> (v <*> w)
    it('honours the composition law', () => {
      type NbFn = (a: number) => number
      const compose: (f: NbFn) => (g: NbFn) => (h: NbFn) => NbFn = f => g => h => x => f(g(h(x)))
      const increment: NbFn = (a: number): number => (+a + 1)
      const Increment = Success.pure(increment)
      const double: NbFn = (a: number): number => (+2 * a)
      const Double = Success.pure(double)
      const minus100: NbFn = (a: number): number => (+a - 100)
      const Minus100 = Success.pure(minus100)
      const composed = compose(increment)(double)(minus100)
      expect(composed(42)).toStrictEqual(-115)
      expect(Success.pure(composed).ap(Success.of(42))).toStrictEqual(Success.of(-115))
      expect(Increment.ap(Double.ap(Minus100.ap(Success.of(42))))).toStrictEqual(Success.of(-115))
      expect(Success.pure(compose).ap(Increment).ap(Double).ap(Minus100).ap(Success.of(42))).toStrictEqual(Success.of(-115))
    })
    // fmap g x = pure g <*> x
    it('ap can do what map does', () => {
      const double: (a: number) => number = a => a * 2
      expect(Success.of(42).map(double)).toStrictEqual(Success.pure(double).ap(Success.of(42)))
    })
  })
  describe('Failure instance', () => {
    // pure id <*> v = v
    it('honours the identity law', () => {
      const identity: <T>(t: T) => T = t => t
      expect(Failure.pure(identity).ap(Failure.pure(42)))
        .toStrictEqual(Failure.pure(42))
    })
    // pure f <*> pure x = pure (f x)
    it('honours the homomorphism law', () => {
      const double: (a: number) => number = a => a * 2
      expect(Failure.pure(double).ap(Failure.pure(42)))
        .toStrictEqual(Failure.pure(double(42)))
    })
    // u <*> pure y = pure ($ y) <*> u
    it('honours the interchange law', () => {
      type NbFn = (a: number) => number
      const double: NbFn = a => 2 * a
      const Double = Failure.pure(double)
      // invert the order
      const interchange: (y: number) => (f: NbFn) => number = y => f => f(y)
      expect(Double.ap(Failure.pure(42)))
        .toStrictEqual(Failure.pure(interchange(42)).ap(Double))
    })
    // pure (.) <*> u <*> v <*> w = u <*> (v <*> w)
    it('honours the composition law', () => {
      type NbFn = (a: number) => number
      const compose: (f: NbFn) => (g: NbFn) => (h: NbFn) => NbFn = f => g => h => x => f(g(h(x)))
      const increment: NbFn = (a: number): number => (+a + 1)
      const Increment = Failure.pure(increment)
      const double: NbFn = (a: number): number => (+2 * a)
      const Double = Failure.pure(double)
      const minus100: NbFn = (a: number): number => (+a - 100)
      const Minus100 = Failure.pure(minus100)
      const composed = compose(increment)(double)(minus100)
      expect(composed(42)).toStrictEqual(-115)
      expect(Failure.pure(composed).ap(Failure.of(42))).toStrictEqual(Failure.of(-115))
      expect(Increment.ap(Double.ap(Minus100.ap(Failure.of(42))))).toStrictEqual(Failure.of(-115))
      expect(Failure.pure(compose).ap(Increment).ap(Double).ap(Minus100).ap(Failure.of(42))).toStrictEqual(Failure.of(-115))
    })
    // fmap g x = pure g <*> x
    it('ap can do what map does', () => {
      const double: (a: number) => number = a => a * 2
      expect(Failure.of(42).map(double)).toStrictEqual(Failure.pure(double).ap(Failure.of(42)))
    })
  })
})

describe('Monad type class', () => {
  describe('Success type instance', () => {
    // unit(x) >>= f === f(x)
    it('honours the left identity law', () => {
      const f: (a: number) => Success<number> = a => Success.return(a / 2)
      expect(Success.return(42).bind(f))
        .toStrictEqual(f(42))
    })
    // ma >>= unit === ma
    it('honours the right identity law', () => {
      const ma = Success.return(42)
      const f: (a: number) => Success<number> = a => Success.return(a / 2)
      const g: (a: number) => Success<number> = a => Success.return(a + 100)
      const expected = (ma.bind(f)).bind(g)
      const actual = ma.bind((t: number) => (f(t).bind(g)))
      expect(actual).toStrictEqual(expected)
    })
    // ma >>= λx -> (f(x) >>= g) === (ma >>= f) >>= g
    it('honours the associativity law', () => {
      const ma = Success.unit(42)
      const f: (a: number) => Success<number> = a => Success.of(a / 2)
      const g: (a: number) => Success<number> = a => Success.of(a + 100)
      const expected = (ma.bind(f)).bind(g)
      const actual = ma.bind((t: number) => (f(t).bind(g)))
      expect(actual).toStrictEqual(expected)
    })
  })
  describe('Failure type instance', () => {
    // unit(x) >>= f === f(x)
    it('honours the left identity law', () => {
      const f: (x: number) => Failure<number> = x => Failure.return(x / 2)
      expect(Failure.return(42).bind(f))
        .toStrictEqual(f(42))
    })
    // ma >>= unit === ma
    it('honours the right identity law', () => {
      const ma = Failure.return(42)
      const f: (x: number) => Failure<number> = x => Failure.of(x / 2)
      const g: (x: number) => Failure<number> = x => Failure.of(x + 100)
      const expected = (ma.bind<number>(f)).bind<number>(g)
      const actual = ma.bind<number>((x: number) => (f(x).bind<number>(g)))
      expect(actual).toStrictEqual(expected)
    })
    // ma >>= λx -> (f(x) >>= g) === (ma >>= f) >>= g
    it('honours the associativity law', () => {
      const ma = Failure.unit(42)
      const f: (x: number) => Failure<number> = x => Failure.of(x / 2)
      const g: (x: number) => Failure<number> = x => Failure.of(x + 100)
      const actual = ma.bind((t: number) => (f(t).bind(g)))
      const expected = (ma.bind(f)).bind(g)
      expect(actual).toStrictEqual(expected)
    })
  })
})
