# Monads

> Free, Libre and Open Source forever.

## License

See LICENSE file.

## Raison d'être

This monorepo will host Typescript implementation of Monads needed by Leading Works SàRL.
They will be Free, Libre and Open Source forever, using the GNU Lesser General Public License (see the LICENSE file, [this website](https://choosealicense.com/licenses/lgpl-3.0), [TL;DR Legal](https://tldrlegal.com/license/gnu-lesser-general-public-license-v3-(lgpl-3)) or [the license on GNU.org](https://www.gnu.org/licenses/lgpl-3.0.html)).

### What is a monad?

> A monad is just a monoid in the category of the endofunctor.

A monad in Haskell is a typeclass that implements a behaviour for generic types.

From the [Learn You a Haskell for Great Good](http://learnyouahaskell.com/) book:

```haskell
class Monad m where
    return :: a -> m a
    (>>=) :: m a -> (a -> m b) -> m         -- bind / flatmap / chain
    (>>) :: m a -> m b -> m                 -- banana on a wire p 278
    x >> y = x >>= \_ -> y

    fail :: String -> m a
    fail msg = error msg

-- example with Maybe as an instance of Monad
instance Monad Maybe where
    return x = Just x
    Nothing >>= f = Nothing
    Just x >>= f  = f x
    fail _ = Nothing
```

and [Professor Frisby's Mostly Adequate Guide to Functional Programming](https://mostly-adequate.gitbook.io/mostly-adequate-guide/) book:

> Monads are pointed functors that can flatten

From the [Fantasy-Land Specification](https://github.com/fantasyland/fantasy-land) :

> Monad
> A value that implements the Monad specification must also implement the Applicative and Chain specifications.
>
>   M['fantasy-land/of'](a)['fantasy-land/chain'](f) is equivalent to f(a) (left identity)
>   m['fantasy-land/chain'](M['fantasy-land/of']) is equivalent to m (right identity)

Where the Applicative and Chain specification are:

```
Applicative

A value that implements the Applicative specification must also implement the Apply specification.

    v['fantasy-land/ap'](A['fantasy-land/of'](x => x)) is equivalent to v (identity)
    A['fantasy-land/of'](x)['fantasy-land/ap'](A['fantasy-land/of'](f)) is equivalent to A['fantasy-land/of'](f(x)) (homomorphism)
    A['fantasy-land/of'](y)['fantasy-land/ap'](u) is equivalent to u['fantasy-land/ap'](A['fantasy-land/of'](f => f(y))) (interchange)

fantasy-land/of method

fantasy-land/of :: Applicative f => a -> f a

A value which has an Applicative must provide a fantasy-land/of function on its type representative. The fantasy-land/of function takes one argument:

F['fantasy-land/of'](a)

Given a value f, one can access its type representative via the constructor property:

f.constructor['fantasy-land/of'](a)

    fantasy-land/of must provide a value of the same Applicative
        No parts of a should be checked
```

where Apply specification is:

```
Apply

A value that implements the Apply specification must also implement the Functor specification.

    v['fantasy-land/ap'](u['fantasy-land/ap'](a['fantasy-land/map'](f => g => x => f(g(x))))) is equivalent to v['fantasy-land/ap'](u)['fantasy-land/ap'](a) (composition)

fantasy-land/ap method

fantasy-land/ap :: Apply f => f a ~> f (a -> b) -> f b

A value which has an Apply must provide a fantasy-land/ap method. The fantasy-land/ap method takes one argument:

a['fantasy-land/ap'](b)

    b must be an Apply of a function
        If b does not represent a function, the behaviour of fantasy-land/ap is unspecified.
        b must be same Apply as a.

    a must be an Apply of any value

    fantasy-land/ap must apply the function in Apply b to the value in Apply a
        No parts of return value of that function should be checked.

    The Apply returned by fantasy-land/ap must be the same as a and b
```

where Chain specification is:

```
Chain

A value that implements the Chain specification must also implement the Apply specification.

    m['fantasy-land/chain'](f)['fantasy-land/chain'](g) is equivalent to m['fantasy-land/chain'](x => f(x)['fantasy-land/chain'](g)) (associativity)

fantasy-land/chain method

fantasy-land/chain :: Chain m => m a ~> (a -> m b) -> m b

A value which has a Chain must provide a fantasy-land/chain method. The fantasy-land/chain method takes one argument:

m['fantasy-land/chain'](f)

    f must be a function which returns a value
        If f is not a function, the behaviour of fantasy-land/chain is unspecified.
        f must return a value of the same Chain

    fantasy-land/chain must return a value of the same Chain
```

and the Functor specification is:

```
Functor

    u['fantasy-land/map'](a => a) is equivalent to u (identity)
    u['fantasy-land/map'](x => f(g(x))) is equivalent to u['fantasy-land/map'](g)['fantasy-land/map'](f) (composition)

fantasy-land/map method

fantasy-land/map :: Functor f => f a ~> (a -> b) -> f b

A value which has a Functor must provide a fantasy-land/map method. The fantasy-land/map method takes one argument:

u['fantasy-land/map'](f)

    f must be a function,
        If f is not a function, the behaviour of fantasy-land/map is unspecified.
        f can return any value.
        No parts of f's return value should be checked.

    fantasy-land/map must return a value of the same Functor
```

In summary, Fantasy Land Specification proposes:

1. A Monad implements Applicative;
2. A Monad implements Chain;
3. Applicative and Chain implement Apply;
4. Apply implements Functor.

Therefore, it stands to reason that a Monad should pass quality gates for:
1. the Monad specification;
2. the Applicative specification;
3. the Chain specification;
4. the Apply specification;
5. the Functor specification.

In order to keep things simple, we will implement each Monad in isolation, so that they can be used in the same quality and without side effects.
Thorough unit tests will deliver safety and trust in the code.

#### Monadic laws

In order to be deemed a Monad, the construct must obey the three monadic laws:

1. Left identity: `return a >>= f ≡ f a`:
   (wrap an `a` in a Monad, then bind a function `f`; is equivalent to calling `f` on `a` in the first place);
2. Right identity: `m >>= return ≡ m`
   (if we have an `a` wrapped in a Monad `m`, then bind it with the Monad's `return`/`of` function, we get back the same `a` wrapped in the Monad `m`);
3. Associativity: `(m >>= f) >>= g ≡ m >>= (\x -> f x >>= g)`
   (if you bind a function `f` to a Monad `m`, then bind the result to a function `g`; it's equivalent to binding a monad `m` to a function that binds `g` to `f`: you can compose/nest however you want).

## Publication

> Consider also the Gitlab registry: https://docs.gitlab.com/ee/user/packages/npm_registry/index.html#publish-an-npm-package-by-using-cicd
>
> https://zellwk.com/blog/publish-to-npm/

## Infra

This will be a mono repo of monadic libs for Typescript.

### Monorepo approach

See Vercel Turborepo

#### Build

To build all apps and packages, run the following command:

```
cd my-turborepo
yarn run build
```

#### Develop

To develop all apps and packages, run the following command:

```
cd my-turborepo
yarn run dev
```

#### Remote Caching

Turborepo can use a technique known as [Remote Caching (Beta)](https://turborepo.org/docs/core-concepts/remote-caching) to share cache artifacts across machines, enabling you to share build caches with your team and CI/CD pipelines.

By default, Turborepo will cache locally. To enable Remote Caching (Beta) you will need an account with Vercel. If you don't have an account you can [create one](https://vercel.com/signup), then enter the following commands:

```
cd my-turborepo
npx turbo login
```

This will authenticate the Turborepo CLI with your [Vercel account](https://vercel.com/docs/concepts/personal-accounts/overview).

Next, you can link your Turborepo to your Remote Cache by running the following command from the root of your turborepo:

```
npx turbo link
```

#### Useful Links

Learn more about the power of Turborepo:

- [Pipelines](https://turborepo.org/docs/core-concepts/pipelines)
- [Caching](https://turborepo.org/docs/core-concepts/caching)
- [Remote Caching (Beta)](https://turborepo.org/docs/core-concepts/remote-caching)
- [Scoped Tasks](https://turborepo.org/docs/core-concepts/scopes)
- [Configuration Options](https://turborepo.org/docs/reference/configuration)
- [CLI Usage](https://turborepo.org/docs/reference/command-line-reference)
