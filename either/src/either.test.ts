import { sum } from './either'

describe('dummy test suite', () => {
  it('passes', () => {
    expect(sum(3, 2)).toBe(5)
  })
})
