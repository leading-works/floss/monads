/**
 * maybe provide a maybe monad typescript implementation
 * maintainer: Soriyath Straessle <soriyath@leading.works>
 * Copyright (C) 2022 Leading Works SàRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Just, Nothing, Maybe } from './maybe'

describe('Functor typeclass', () => {
  describe('fmap :: (a -> b) -> f a -> f b', () => {
    [
      { label: 'number', fixture: 42, f: (t: number) => t / 2, expected: 21 },
      { label: 'number', fixture: NaN, f: (t: number) => t / 2, expected: NaN },
      { label: 'number', fixture: Infinity, f: (t: number) => t / 2, expected: Infinity },
      { label: 'number', fixture: -Infinity, f: (t: number) => t / 2, expected: -Infinity },
      { label: 'number', fixture: +0, f: (t: number) => t / 2, expected: 0 },
      { label: 'string', fixture: 'some text', f: (t: string) => t.toUpperCase(), expected: 'SOME TEXT' },
      { label: 'boolean', fixture: true, f: (t: boolean) => !t, expected: false },
      { label: 'boolean', fixture: false, f: (t: boolean) => !t, expected: true },
      { label: 'Array<number>', fixture: [0, 1, 2, 3], f: (t: number[]) => [...t, 4], expected: [0, 1, 2, 3, 4] },
      { label: 'Array<string>', fixture: ['a', 'b', 'c'], f: (t: number[]) => [...t, 'd'], expected: ['a', 'b', 'c', 'd'] },
      { label: 'Array<boolean>', fixture: [true, false, true], f: (t: number[]) => [...t, true], expected: [true, false, true, true] },
      { label: 'Object', fixture: { a: 42 }, f: (t: Object) => Object.assign({}, t, { b: 'text' }), expected: { a: 42, b: 'text' } }
    ].forEach(({ label, fixture, f, expected }) => {
      it(`passes the quality gate with ${label} type.`, () => {
        const arrange: Maybe<typeof fixture> = Just.of<typeof fixture>(fixture)
        const actual = arrange.map<typeof fixture>(f)
        expect(actual.isPresent()).toBe(true)
        expect(actual.unwrap()).toStrictEqual(expected)
        expect(actual.orElse<string>('random dummy text')).toStrictEqual(expected)
        const nothing: Maybe<typeof fixture> = Nothing.of(fixture)
        expect(nothing.isPresent()).toBe(false)
        expect(nothing.unwrap()).toStrictEqual(null)
        expect(nothing.orElse<string>('text')).toStrictEqual('text')
      })
    })
    it('works with a mapped function that transforms the data from type number to type string.', () => {
      const arrange: Maybe<number> = Just.of<number>(84)
      const actual: Maybe<string> = arrange.map<string>((t: number) => String.fromCharCode(t))
      expect(actual.unwrap()).toStrictEqual('T')
      expect(actual.isPresent()).toBe(true)
      expect(actual.orElse<string>('random dummy text')).toStrictEqual('T')
      const nothing: Maybe<number> = Nothing.of(84)
      expect(nothing.unwrap()).toStrictEqual(null)
      expect(nothing.isPresent()).toBe(false)
      expect(nothing.orElse<string>('text')).toStrictEqual('text')
    })
    it('works with a mapped function that transforms the data from type string to type numbr.', () => {
      const arrange: Maybe<string> = Just.of<string>('*')
      const actual: Maybe<number> = arrange.map<number>((t: string) => t.codePointAt(0))
      expect(actual.unwrap()).toStrictEqual(42)
      expect(actual.isPresent()).toBe(true)
      expect(actual.orElse<string>('random dummy text')).toStrictEqual(42)
      const nothing: Maybe<string> = Nothing.of('*')
      expect(nothing.unwrap()).toStrictEqual(null)
      expect(nothing.isPresent()).toBe(false)
      expect(nothing.orElse<string>('text')).toStrictEqual('text')
    })
  })
  describe('Functor laws', () => {
    [
      { label: 'number', fixture: 42 },
      { label: 'number', fixture: NaN },
      { label: 'number', fixture: Infinity },
      { label: 'number', fixture: -Infinity },
      { label: 'number', fixture: +0 },
      { label: 'string', fixture: 'some text' },
      { label: 'boolean', fixture: true },
      { label: 'boolean', fixture: false },
      { label: 'Array<number>', fixture: [0, 1, 2, 3] },
      { label: 'Array<string>', fixture: ['a', 'b', 'c'] },
      { label: 'Array<boolean>', fixture: [true, false, true] },
      { label: 'Object', fixture: { a: 42 } }
    ].forEach(({ label, fixture }) => {
      it(`first law / identity law works with Maybe<${label}>`, () => {
        const identity: <T>(t: T) => T = t => t
        const just: Maybe<typeof fixture> = Just.of<typeof fixture>(fixture)
        const actual: Maybe<typeof fixture> = just.map<typeof fixture>(identity)
        expect(Just.of(fixture)).toStrictEqual(Just.of(fixture).map(identity))
        expect(Just.of(identity(fixture))).toStrictEqual(Just.of(fixture))
        expect(Just.of(identity(fixture))).toStrictEqual(Just.of(fixture).map(identity))
        expect(actual).toStrictEqual(just)
        expect(identity(actual)).toStrictEqual(just.map(identity))
      })
    });
    [
      { label: 'number', fixture: 42 },
      { label: 'number', fixture: NaN },
      { label: 'number', fixture: Infinity },
      { label: 'number', fixture: -Infinity },
      { label: 'number', fixture: +0 }
    ].forEach(({ label, fixture }) => {
      it(`second law / associativity law works with Maybe<${label}>`, () => {
        type NbFn = (x: number) => number
        const double: NbFn = t => 2 * t
        const increment: NbFn = t => t + 1
        const compose: (f: NbFn) => (g: NbFn) => NbFn = f => g => x => f(g(x))
        const plusOneThenDouble: NbFn = compose(double)(increment)
        const just = Just<number>.of(fixture)
        const actual = just.map(plusOneThenDouble)
        expect(actual).toStrictEqual(just.map(increment).map(double))
        const nothing = Nothing.of(fixture)
        const actual2 = nothing.map(plusOneThenDouble)
        expect(actual2).toStrictEqual(nothing.map(increment).map(double))
        expect(actual2.isPresent()).toStrictEqual(false)
      })
    });
    [
      { label: 'string', fixture: 'some text', other: 'new', expected: 'SOME TEXT new' }
    ].forEach(({ label, fixture, other, expected }) => {
      it(`second law / associativity law works with Maybe<${label}>`, () => {
          type StringFn = (t: string) => string
          const allCaps: StringFn = t => t.toUpperCase()
          const addOther: StringFn = t => `${t} ${other}`
          const compose: (f: StringFn) => (g: StringFn) => StringFn = f => g => x => f(g(x))
          const allCapsThenAdd: StringFn = compose(addOther)(allCaps)
          const just = Just<string>.of(fixture)
          const actual = just.map(allCapsThenAdd)
          expect(actual).toStrictEqual(just.map(allCaps).map(addOther))
          expect(actual).toStrictEqual(Just.of(expected))
          expect(actual).not.toStrictEqual(just.map(addOther).map(allCaps))
          const nothing = Nothing.of(fixture)
          const actual2 = nothing.map(allCapsThenAdd)
          expect(actual2).toStrictEqual(nothing.map(allCaps).map(addOther))
          expect(actual).not.toStrictEqual(just.map(addOther).map(allCaps))
          expect(actual2.isPresent()).toStrictEqual(false)
      })
    });
    [
      { label: 'Array<number>', fixture: [0, 1, 2, 3], expected: [3, 2, 1, 0] },
      { label: 'Array<string>', fixture: ['a', 'b', 'c'], expected: ['c', 'b'] }
    ].forEach(({ label, fixture }) => {
      it(`second law / associativity law works with Maybe<${label}>`, () => {
        type ArrayFn = (t: any[]) => any[]
        const compose: (f: ArrayFn) => (g: ArrayFn) => ArrayFn = f => g => x => f(g(x))
        const revert: ArrayFn = (xs) => [...xs].reverse() // reverse mutates... protecting against this side effect with spread operator
        const sliceLast: ArrayFn = (xs) => xs.slice(0, -1)
        const revertThenSliceLast = compose(sliceLast)(revert)
        const just: Maybe<typeof fixture> = Just.of<typeof fixture>(fixture)
        const actual = just.map(revertThenSliceLast)
        expect(actual).toStrictEqual(just.map(revert).map(sliceLast))
        const nothing = Nothing.of(fixture)
        const actual2 = nothing.map(revertThenSliceLast)
        expect(actual2).toStrictEqual(nothing.map(revert).map(sliceLast))
        expect(actual).not.toStrictEqual(just.map(sliceLast).map(revert))
        expect(actual2.isPresent()).toStrictEqual(false)
      })
    })
  })
})

/**
 * @see https://en.wikibooks.org/wiki/Haskell/Applicative_functors
 */
describe('Applicative typeclass', () => {
  it('applys a function in a context to two primitives in a context.', () => {
    const One = Just.of(1)
    const Two = Just.of(2)
    const Add = Just.of((a: number) => (b: number) => a + b)
    expect(Add.ap(One).ap(Two)).toStrictEqual(Just.of(3))
  })
  // pure id <*> v = v                            -- Identity
  it('honors the identity law', () => {
    const identity: <T>(t: T) => T = t => t
    expect(Just.pure(identity).ap(Just.of(42))).toStrictEqual(Just.of(42))
  })
  // pure f <*> pure x = pure (f x)               -- Homomorphism
  it('honors the homomorphism law', () => {
    const double: (a: number) => number = a => 2 * a
    expect(Just.pure(double).ap(Just.pure(42))).toStrictEqual(Just.pure(double(42)))
  })
  // u <*> pure y = pure ($ y) <*> u              -- Interchange
  it('honors the interchange law', () => {
    type NbFn = (x: number) => number
    const double: NbFn = x => 2 * x
    const Double = Just.pure(double)
    const supply = (y: number) => (f: NbFn) => f(y)
    const rightHandSide = Just.pure(supply(42)).ap(Double)
    expect(Double.ap(Just.pure(42))).toStrictEqual(rightHandSide)
  })
  // pure (.) <*> u <*> v <*> w = u <*> (v <*> w) -- Composition
  it('honors the composition law', () => {
    type NbFn = (x: number) => number
    const compose: (f: NbFn) => (g: NbFn) => (h: NbFn) => NbFn = f => g => h => x => f(g(h(x)))
    const increment: NbFn = (a: number): number => (+a + 1)
    const Increment = Just.pure(increment)
    const double: NbFn = (a: number): number => (+2 * a)
    const Double = Just.pure(double)
    const minus100: NbFn = (a: number): number => (+a - 100)
    const Minus100 = Just.pure(minus100)
    const composed = compose(increment)(double)(minus100)
    expect(composed(42)).toStrictEqual(-115)
    expect(Just.pure(composed).ap(Just.of(42))).toStrictEqual(Just.of(-115))
    expect(Increment.ap(Double.ap(Minus100.ap(Just.of(42))))).toStrictEqual(Just.of(-115))
    expect(Just.pure(compose).ap(Increment).ap(Double).ap(Minus100).ap(Just.of(42))).toStrictEqual(Just.of(-115))
  })
  it('liftsA2 works', () => {
    function sum (a: number, b: number): number { return a + b }
    expect(Just.of(20).liftA2(sum, Just.of(22))).toStrictEqual(Just.of(42))
  })
  it('liftsA3 works', () => {
    function sum (a: number, b: number, c: number): number { return a + b + c }
    expect(Just.of(20).liftA3(sum, Just.of(22), Just.of(-42))).toStrictEqual(Just.of(0))
  })
  it('liftsA4 works', () => {
    function sum (a: number, b: number, c: number, d: number): number { return a + b + c + d }
    expect(Just.of(20).liftA4(sum, Just.of(22), Just.of(-42), Just.of(-100))).toStrictEqual(Just.of(-100))
  })
  it('liftsA5 works', () => {
    function sum (a: number, b: number, c: number, d: number, e: number): number { return a + b + c + d + e }
    expect(Just.of(20).liftA5(sum, Just.of(22), Just.of(-42), Just.of(-100), Just.of(1000))).toStrictEqual(Just.of(900))
  })
  // fmap g x = pure g <*> x
  it('ap can do what map does', () => {
    const double: (a: number) => number = a => a * 2
    expect(Just.of(42).map(double)).toStrictEqual(Just.pure(double).ap(Just.of(42)))
  })
})

describe('Monad typeclass', () => {
  describe('Maybe -> Just<T>', () => {
    // unit is a left-identity for bind: unit(x) >>= f === f(x)
    it('honours the left identity law', () => {
      const f: (a: number) => Just<number> = a => Just.of(a / 2)
      const expected = f(42)
      const actual = Just.unit(42).bind(f)
      expect(actual).toStrictEqual(expected)
    })
    // unit is also a right-identity for bind: ma >>= unit === ma
    it('honours the right identity law', () => {
      const ma = Just.unit(42)
      const expected = ma
      const actual = ma.bind(Just.unit)
      expect(actual).toStrictEqual(expected)
    })
    // bind is essentially associative: ma >>= λx -> (f(x) >>= g) === (ma >>= f) >>= g
    it('honours the associativity law', () => {
      const ma = Just.unit(42)
      const f: (a: number) => Just<number> = a => Just.of(a / 2)
      const g: (a: number) => Just<number> = a => Just.of(a + 100)
      const expected = (ma.bind(f)).bind(g)
      const actual = ma.bind((t: number) => (f(t).bind(g)))
      expect(actual).toStrictEqual(expected)
    })
  })
  describe('Maybe -> Nothing', () => {
    // unit is a left-identity for bind: unit(x) >>= f === f(x)
    it('honours the left identity law', () => {
      const f: (a: number) => Nothing = a => Nothing.of(a / 2)
      const expected = f(42)
      const actual = Nothing.unit(42).bind(f)
      expect(actual).toStrictEqual(expected)
    })
    // unit is also a right-identity for bind: ma >>= unit === ma
    it('honours the right identity law', () => {
      const ma = Nothing.unit(42)
      const expected = ma
      const actual = ma.bind(Nothing.unit)
      expect(actual).toStrictEqual(expected)
    })
    // bind is essentially associative: ma >>= λx -> (f(x) >>= g) === (ma >>= f) >>= g
    it('honours the associativity law', () => {
      const ma = Nothing.unit(42)
      const f: (a: number) => Nothing = a => Nothing.of(a / 2)
      const g: (a: number) => Nothing = a => Nothing.of(a + 100)
      const expected = (ma.bind(f)).bind(g)
      const actual = ma.bind((t: number) => (f(t).bind(g)))
      expect(actual).toStrictEqual(expected)
    })
  })
})

describe('Utility stuff', () => {
  it('Nothing.ness factory method works', () => {
    expect(Nothing.ness).toStrictEqual(Nothing.of(null))
  })
})
