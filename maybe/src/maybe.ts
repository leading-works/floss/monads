/**
 * Maybe provide a Maybe Monad Typescript implementations
 * maintainer: Soriyath Straessle <soriyath@leading.works>
 * Copyright (C) 2022 Leading Works SàRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export type Maybe<T> = Just<T> | Nothing

export class Just<T> {
  // #region Properties and constructor
  __value__: T

  constructor (value: T) {
    this.__value__ = value
  }
  // #endregion

  inspect <T>(): Just<T> {
    console.log(`Just<${typeof this.__value__}>(${(this.__value__) as unknown as string})`)
    return this as unknown as Just<T> // Make it fluent for debugging purposes
  }

  /**
   * fantasy-land/map :: Functor f => f a ~> (a -> b) -> f b
   *
   * A value which has a Functor must provide a fantasy-land/map method. The fantasy-land/map method takes one argument:
   *
   * u['fantasy-land/map'](f)
   *
   *   1. f must be a function,
   *        i. If f is not a function, the behaviour of fantasy-land/map is unspecified.
   *       ii. f can return any value.
   *      iii. No parts of f's return value should be checked.
   *
   *   2. fantasy-land/map must return a value of the same Functor
   */
  map <U>(f: Function): Just<U> {
    return Just.of(f(this.__value__))
  }

  /**
   * @see https://en.wikibooks.org/wiki/Haskell/Applicative_functors
   */
  ap <B>(b: Maybe<B>): Just<B> {
    return Just.of((this.unwrap() as unknown as Function)(b.unwrap()))
  }

  liftA2 <T>(fn: Function, f2: Maybe<T>): Maybe<T> {
    const curried: (a: T) => (b: T) => T = a => b => fn(a, b)
    return this.map(curried).ap(f2 as Just<T>)
  }

  liftA3 <T>(fn: Function, f2: Maybe<T>, f3: Maybe<T>): Maybe<T> {
    const curried: (a: T) => (b: T) => (c: T) => T = a => b => c => fn(a, b, c)
    return this.map(curried).ap(f2 as Just<T>).ap(f3 as Just<T>)
  }

  liftA4 <T>(fn: Function, f2: Maybe<T>, f3: Maybe<T>, f4: Maybe<T>): Maybe<T> {
    const curried: (a: T) => (b: T) => (c: T) => (d: T) => T = a => b => c => d => fn(a, b, c, d)
    return this.map(curried).ap(f2 as Just<T>).ap(f3 as Just<T>).ap(f4 as Just<T>)
  }

  liftA5 <T>(fn: Function, f2: Maybe<T>, f3: Maybe<T>, f4: Maybe<T>, f5: Maybe<T>): Maybe<T> {
    const curried: (a: T) => (b: T) => (c: T) => (d: T) => (e: T) => T = a => b => c => d => e => fn(a, b, c, d, e)
    return this.map(curried).ap(f2 as Just<T>).ap(f3 as Just<T>).ap(f4 as Just<T>).ap(f5 as Just<T>)
  }

  isPresent (): boolean {
    return true
  }

  orElse <U>(_defaultValue: U): U {
    return this.__value__ as unknown as U
  }

  isNothing (): boolean {
    return false
  }

  join (): T {
    return this.__value__
  }

  unwrap (): T {
    return this.__value__
  }

  chain (f: Function): Maybe<T> {
    return this.map(f).join() as Maybe<T>
  }

  bind = this.chain

  flatMap = this.chain

  of = Just.of

  pure = Just.pure

  unit = Just.unit

  static of<T>(value: T): Just<T> {
    return new Just(value)
  }

  static return<T>(value: T): Just<T> {
    return new Just(value)
  }

  static pure<T>(value: T): Just<T> {
    return new Just(value)
  }

  static unit<T>(value: T): Just<T> {
    return new Just(value)
  }
}

export class Nothing {
  __value__ = null

  constructor (_value: any) {
    this.__value__ = null
  }

  inspect (): Nothing {
    console.info('Nothing')
    return this as unknown as Nothing // make it fluent for debugging purposes
  }

  map (_f: Function): Nothing {
    return this
  }

  ap<B>(_f: Just<Function>): Maybe<B> {
    return this as Nothing
  }

  liftA2 <T>(_fn: Function, _f2: Maybe<T>): Maybe<T> {
    return this as Nothing
  }

  liftA3 <T>(_fn: Function, _f2: Maybe<T>, _f3: Maybe<T>): Maybe<T> {
    return this as Nothing
  }

  liftA4 <T>(_fn: Function, _f2: Maybe<T>, _f3: Maybe<T>, _f4: Maybe<T>): Maybe<T> {
    return this as Nothing
  }

  liftA5 <T>(_fn: Function, _f2: Maybe<T>, _f3: Maybe<T>, _f4: Maybe<T>, _f5: Maybe<T>): Maybe<T> {
    return this as Nothing
  }

  isPresent (): boolean {
    return false
  }

  orElse <U>(defaultValue: U): U {
    return defaultValue
  }

  isNothing (): boolean {
    return true
  }

  join (): null {
    return null
  }

  unwrap (): null {
    return null
  }

  chain (_f: Function): Nothing {
    return this
  }

  bind = this.chain

  flatMap = this.chain

  of = Nothing.of

  pure = Nothing.pure

  unit = Nothing.unit

  static ness: Nothing = Nothing.of(null)

  static of (value: any): Nothing {
    return new Nothing(value)
  }

  static return (value: any): Nothing {
    return new Nothing(value)
  }

  static pure (value: any): Nothing {
    return new Nothing(value)
  }

  static unit (value: any): Nothing {
    return new Nothing(value)
  }
}
